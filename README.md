# Berechnungsmodell-Simulator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Ziel meiner Arbeit

Ziel meiner Arbeit ist, einen zeitgemäßen und interaktiven Berechnungsmodell-Simulator zu entwickeln. Dabei wird ein veralteter Java-Simulator durch eine moderne Webanwendung im TypeScript-Format abgelöst. Durch gezielte Verbesserungen, die Entwicklung einer interaktiven Lernumgebung und die Optimierung der Benutzerfreundlichkeit strebe ich danach, Studierenden einen praxisnahen Zugang zu den Grundprinzipien von Maschinencodes zu bieten. Der Umstieg auf eine Webanwendung soll dabei einen einfachen Zugang ermöglichen, indem aufwendige Installationsprozesse vermieden werden.

## Komponenten des Berechnungsmodell-Simulators

### 1. Speicher
Der Speicher hat eine Größe von 256 Wörtern, wobei jedes Wort 16 Bit enthält. Daten werden unter Verwendung von Adressen (ADDR) im Speicher gespeichert. Alle Aktivitäten der Bottom-Funktionen werden im Speicher angezeigt.

### 2. Register
Registers sind Komponenten, die eine Bitfolge ähnlich wie ein Wort im Hauptspeicher enthalten. Sie dienen zur Speicherung von Zwischenergebnissen während der Berechnung. Die VM-Simulation verfügt über 16 Register, nummeriert von 0 bis F.

### 3. ALU (Arithmetic Logic Unit)
Die ALU führt arithmetische und logische Operationen durch, um Berechnungen innerhalb des Simulators durchzuführen.

### 4. Befehlszähler (Programmzähler)
Der 8-Bit Program Counter ist ein internes Register, das die Adresse des nächsten auszuführenden Befehls speichert.

### 5. Instruktionsregister
Das 16-Bit Instructions Register (IR) ist ein internes Register, das den aktuell ausgeführten Befehl enthält.

### 6. Instruktionen

Die Instruktionen sind in zwei Formaten dargestellt:

- Format 1: [op | d | s | t]       (op, d, s, t: jeweils 4-Bit)
- Format 2: [op | d | st]       (op, d: 4-Bit, st: 8-Bit)

#### Instruktionen-Set:

1. Halt-Instruktion
  - Code: 0
  - Operation: halt

2. Arithmetische Instruktionen:
  - Code 1: add         Operation: R[d] <- R[s] + R[t]
  - Code 2: subtract      Operation: R[d] <- R[s] - R[t]

3. Logische Instruktionen:
  - Code 3: and          Operation: R[d] <- R[s] & R[t]
  - Code 4: xor           Operation: R[d] <- R[s] ^ R[t]
  - Code 5: shift left   Operation: R[d] <- R[s] << R[t]
  - Code 6: shift right  Operation: R[d] <- R[s] >> R[t]

4. Speicher-Instruktionen:
  - Code 8: load             Operation: R[d] <- M[addr]
  - Code 9: store             Operation: M[addr] <- R[d]

5. Speicher-Adresse-Instruktionen:
  - Code 7: load address     Operation: R[d] <- addr
  - Code A: load indirect   Operation: R[d] <- M[R[t]]
  - Code B: store indirect  Operation: M[R[t]] <- R[d]

6. Kontrollfluss-Instruktionen:
  - Code C: branch zero         Operation: if (R[d] == 0) PC <- addr
  - Code D: branch positive  Operation: if (R[d] > 0) PC <- addr
  - Code E: jump register      Operation: PC <- R[d]
  - Code F: jump and link      Operation: R[d] <- PC; PC <- addr


## Benutzeroberfläche

### Schaltflächen
- Ausführen
- Schritt
- Rücksetzen
- Laden
- Suchen
- Text Laden
- Text Speichern
- Datei auswählen
- Submit für Standard Eingabe

### Tabellen
- Speicher und Daten-tabelle
- Register-Tabelle
- Aktueller PC und IR-Tabelle

## Installation

Die Installation des Simulators ist nicht erforderlich. Besuchen Sie einfach die [BEMOS-Webseite](https://aalh57.git-pages.thm.de/vm_simulator).

## Verwendete Technologien

- TypeScript
- HTML
- CSS
- Angular-Framework
- Bootstrap


## Lizenz

Dieses Projekt ist unter der [Lizenz] lizenziert. Weitere Details finden Sie in der Datei LICENSE.



## Running unit tests:

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## LICENSE
Das Projekt kann von Studierenden an der THM weiterentwickeln. <br>
License ist frei für alle Studierende 
