import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {WmMachine} from "../model/wm-machine";
import {WmActions} from "../model/wm-actions";
import {WmStatus} from "../model/wm-status";

@Component({
  selector: 'app-binary-input',
  templateUrl: './binary-input.component.html',
  styleUrls: ['./binary-input.component.css']
})
export class BinaryInputComponent {
  title = 'simulator';
  public fileContent: string | undefined = '';
  private regex = /[0-9A-F]/g;
  @ViewChild('fileUploader') fileUploader!: ElementRef;
  @ViewChild('textInput') textInput!: ElementRef;

  @Input()
  public wmMachine: WmMachine | undefined;

  @Output()
  wmAction: EventEmitter<WmActions> = new EventEmitter<WmActions>();

  @Output()
  stdinEvent: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  loadListEmitter: EventEmitter<Map<string, string>> = new EventEmitter<Map<string, string>>();

  @Output()
  saveListEmitter: EventEmitter<string> = new EventEmitter<string>();

  public constructor() {
  }

  /**
   isSelected Funktion zeigt, ob ein addres selected oder nicht
   @param {number} addres - addres number param.
   @param {number} currentPc - currentPc number param.
   @returns {boolean} This is the result
   */
  public isSelected(addres: number, currentPc: number): boolean {
    return addres == currentPc;
  }

  /**
   onChange Funktion startet, wenn auf das Event Load_File geklickt.
   dann wird das File behandelt.
   @param {Event} event - event Event param.
   */
  public onChange(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList | null = element.files;
    if (fileList && fileList.length > 0) {
      let fileReader: FileReader = new FileReader();
      let self = this;

      fileReader.onloadend = function (x) {
        self.fileContent = fileReader.result ? fileReader.result.toString() : '';
        if (!self.isInputValid(self.fileContent) || self.fileContent === '') {
          self.fileUploader.nativeElement.value = null;
          self.textInput.nativeElement.value = null;
        } else {
          self.textInput.nativeElement.value = self.fileContent;
          self.loadList(self.getAdjustedList(self.fileContent));
          self.fileUploader.nativeElement.value = null;
        }
      }
      fileReader.readAsText(fileList[0]);
    }
  }

  /**
   * isInputValid Funktion hat typ String und in dieser Funktion wird überprüft, ob
   * die Eingabe Text richtig bzw. valid ist.
   * @param {String} inputValue - inputValue String param.
   * @returns {boolean} This is the result
   */
  private isInputValid(inputValue: string): boolean {
    let rows = inputValue.split('\n');
    let result = true;

    rows.filter(line => line.trim() != '')
        .forEach(line => {
          let entry = line.split(':');
          let address = entry[0];
          console.log(entry.length)
          if (entry.length > 1) {
          let instractionAndComments = entry[1].trim().split(' ');
          let ir = instractionAndComments[0];
          if (entry.length < 2 || !this.isHexadecimal(address.trim(), true) || !this.isHexadecimal(ir.trim(), false)) {
            // das bedeutet dass der eingabe mindestens ":" enthält aber incorrect ist
            // b.p: 10:hjgss oder 12:2345:ssas:fasf oder 23:12347
            alert("dieser Zeile: " + line + " falsch.")
            result = false;
          }
        }
          else {
            // invalid format b.p: dfar,sazg oder 234jslaj
            // ohne doppel punkt
            alert("dieser Zeile: " + line + " falsch.")
            result = false;
          }
        })
    return result;
  }

  /**
   *  isHexadecimal Funktion überprüft, ob die Eingabe String_Text in Hexadecimal
   * @param {String} input - input String param.
   * @return {boolean} This is the result
   * @private
   */
  private isHexadecimal(input: string, isAddress: boolean): boolean {
    let bitLength = isAddress ? 2 : 4

    if (input.length > bitLength) {
      return false
    }
    for (let i = 0; i < input.length; i++) {
      if (input.charAt(i).match(this.regex) === null) {
        return false;
      }
    }
    return true;
  }

  /**
   * getAdjustedList Funktion nimmt ein String_Text, aber durch die Funktion wird
   * Das Text in zeilen geteilt.
   * Jede Zeile hat Addres und die Instruction Register
   * @param {String} text - text String param.
   * @private
   * @return {String} This is the result
   */
  public getAdjustedList(text: string): Map<string, string> {
    let result: Map<string, string> = new Map<string, string>();
    text.split("\n")
        .filter(line => line.trim() != '')
        .forEach(line => {
          let entry = line.split(":");
          let instractionAndComments = entry[1].trim().split(" ");
          result.set(entry[0].trim(), instractionAndComments[0].trim());
        })
    return result;
  }

  /**
   * Diese Funktion repräsentiert die Number sowohl Positive ale auch negative value
   * in Hexadecimal in 4-Stellen oder 1-Stellen
   * @param {number} value - value number param
   * @param {boolean} is16Bits - is16Bits boolean param
   * @return {String} This is the result
   */
  public getHexadecimalValue(value: number | undefined, is16Bits: boolean): string {
    if (value === undefined) {
      value = 0
    }
    const maxLength: number = is16Bits ? 4 : 1;
    return ((value >>> 0) & 0x0000ffff).toString(16).padStart(maxLength, '0').toUpperCase();
  }

  /**
   * Load Funktion sendet Load_Event zu Parent
   */
  public load() {
    this.wmAction.emit(WmActions.load);
  }

  /**
   * loadText Funktion überprüft, ob die Input Valid. wenn Input Valid sendet die Funktion eine Liste
   * zu Parent bzw. Load Liste als Text.
   * Wenn Input_Text nicht Valid, wird ein Alert angezeigt.
   */
  public loadText() {
    if (this.isInputValid(this.textInput.nativeElement.value)) {
      this.loadList(this.getAdjustedList(this.textInput.nativeElement.value));
    }
  }

  public saveText() {
    if (this.isInputValid(this.textInput.nativeElement.value)) {
      this.saveListEmitter.emit(this.textInput.nativeElement.value);
    }
  }

  /**
   * Sendet eine Liste zur Parent, bzw. eine Load List Event
   * @param {Map} entryList - entryList Map param
   * @private
   */
  private loadList(entryList: Map<string, string>) {
    this.loadListEmitter.emit(entryList);
  }

  /**
   * Look Funktion sendet Look_Event zu Parent
   */
  public look() {
    this.wmAction.emit(WmActions.look);
  }

  public checkWmStatus(): boolean{
    return this.wmMachine?.wmStatus == WmStatus.running ||
      this.wmMachine?.wmStatus == WmStatus.paused;
  }

  /**
   * Step Funktion sendet Step_Event zu Parent
   */
  public step() {
    this.wmAction.emit(WmActions.step);
  }

  /**
   * Run Funktion sendet Run_Event zu Parent
   */
  public run() {
    this.wmAction.emit(WmActions.run);
  }

  /**
   * Reset Funktion sendet Reset_Event zu Parent
   */
  public reset() {
    this.wmAction.emit(WmActions.reset);
  }

  protected readonly WmStatus = WmStatus;
}
