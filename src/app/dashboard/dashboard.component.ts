import {Component, OnInit} from '@angular/core';
import {WmMachine} from "../model/wm-machine";
import {Bit} from "../model/bit";
import {WmActions} from "../model/wm-actions";
import {Nibble} from "../model/nibble";
import {MemoryValue} from "../model/memoryValue";
import {Instractions} from "../model/instractions";
import {WmStatus} from "../model/wm-status";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  title = 'simulator';

  public waitingForClick = false;
  public wmMachine: WmMachine = new WmMachine();

  public ngOnInit(): void {
    this.initAddres();
    this.initPc();
    this.initInstr();
    this.initRegister();
    this.initData();
  }

  /**
   * Initialisierung Adresse
   * Hier wird jedes Bit mit value belegt.
   * Erste Schleife für Nibbel und die zweite Schleife für Bits in den Nibbeln
   * @private
   */
  private initAddres():void {
    let value: number = 1;
    for(let i= this.wmMachine.addres.length -1; i >= 0; i--){
      const bits: Bit[] = new Array(4);

      for (let j = bits.length-1; j >= 0 ; j--){
        bits[j] = new Bit(value,false);
        value = value << 1;
      }
      this.wmMachine.addres[i]= new Nibble(bits,i);
    }
  }

  /**
   * Initialisierung Programm Counter
   * Hier wird jedes Bit mit value belegt.
   * Erste Schleife für Nibbel und die zweite Schleife für Bits in den Nibbeln
   * @private
   */
  private initPc():void {
    let value: number = 1;
    for(let i= this.wmMachine.pc.length - 1 ; i >= 0; i--){
      const bits: Bit[] = new Array(4);

      for (let j = bits.length-1; j >= 0 ; j--){
        bits[j] = new Bit(value,false);
        value = value << 1;
      }
      this.wmMachine.pc[i]= new Nibble(bits,i);
    }
  }

  /**
   * Initialisierung Instruction Register
   * Hier wird jedes Bit mit value belegt.
   * Erste Schleife für Nibbel und die zweite Schleife für Bits in den Nibbeln
   * @private
   */
  private initInstr():void {
    let value: number = 1;
    for(let i=this.wmMachine.instr.length -1 ; i >= 0; i--){
      const bits: Bit[] = new Array(4);

      for (let j = bits.length-1; j >= 0 ; j--){
        bits[j] = new Bit(value,false);
        value = value << 1;
      }
      this.wmMachine.instr[i]= new Nibble(bits,i);
    }
  }

  /**
   * Initialisierung Data
   * Hier wird jedes Bit mit value belegt.
   * Erste Schleife für Nibbel und die zweite Schleife für Bits in den Nibbeln
   * @private
   */
  private initData():void {
    let value: number = 1;
    for(let i=this.wmMachine.data.length -1; i >= 0; i--){
      const bits: Bit[] = new Array(4);

      for (let j = bits.length-1; j >= 0 ; j--){
        bits[j] = new Bit(value,false);
        value = value << 1;
      }
      this.wmMachine.data[i]= new Nibble(bits,i);
    }
  }

  /**
   * Initialisierung Register.
   * Alle Registers werden am Anfang mit 0 angelegt
   */
  private initRegister(){
    this.wmMachine.register.fill(0);
  }


  /**
   * runWmAction wird auf Event bzw. Signatur von kinder gewartet,
   * damit auf dieser Event bzw. Signatur reagiert.
   * @param action
   */
  public runWmAction(action: WmActions){
    switch (action) {
      case WmActions.load:
        this.load();
        break;
      case WmActions.look:
        this.look();
        break;
      case WmActions.step:
        this.step();
        break;
      case WmActions.run:
        this.run();
        break;
      case WmActions.reset:
        this.reset();
        break;
    }
  }

  /**
   * loadList Funktion wird auf  LoadList_Event bzw. LoadList_Signatur von kinder gewartet,
   * damit auf dieser Event bzw. Signatur reagiert.
   * wenn loadList reagiert, zuerst wird ein Reset für Speicher gemacht
   * dann werden die Adressen mit ihren Instruction Register im Memory gespeichert.
   * @param entryList
   */
  public loadList(entryList: Map<string,string>){
    this.reset();
    entryList.forEach((value:string, key: string) =>{
      let memoryValue = new MemoryValue(parseInt(key,16),parseInt(value,16));
      this.storeValue(memoryValue);
    })
  }

  public saveList(data: string){

    const blob = new Blob([data], { type: 'text/plain' });

    const blobUrl = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.href = blobUrl;
    a.download = 'myFile.txt';
    a.click();

    URL.revokeObjectURL(blobUrl);
  }

  /**
   * Load Funktion lagt der Wert bzw. das Data mit der ausgewählten Adresse im Speicher ab.
   * Auf diese Weise können Daten mit Adressen im Speicher gespeichert werden.
   */
  public load():void{
    const addres:number = this.getDecimalValue(this.wmMachine.addres);
    const data: number = this.getDecimalValue(this.wmMachine.data);
    const memoryValue = new MemoryValue(addres,data);

    this.storeValue(memoryValue);
  }

  /**
   * Look Funktion verwendet ADDR, um im Speicher die DATEN  zu zeigen.
   * Wenn die ADDR nicht abgelegt ist, dann wird kein DATA gezeigt.
   */
  public look(): void{
    const addres:number = this.getDecimalValue(this.wmMachine.addres);

    this.wmMachine.currentPc = addres;
    this.wmMachine.currentIr = this.wmMachine.memory.get(addres);
    this.updateWmPc(addres);
    this.wmMachine.currentIr === undefined ? this.updateWmIr(0) : this.updateWmIr(this.wmMachine.currentIr);
    console.log(this.wmMachine.pc[0])
  }

  /**
   * Run Funktion zeigt das Ergebnis von Assemble_Code.
   * Run Funktion endet, nur wenn keinen Step mehr gibt bzw. bis Halt 0000
   */
  public async run(): Promise<void>{
    this.wmMachine.wmStatus = WmStatus.running
    this.wmMachine.currentIr = this.wmMachine.memory.get(this.wmMachine.currentPc)
//    console.log(this.wmMachine.currentIr )
    while (true){
      if(this.wmMachine.currentIr == Instractions.halt || this.wmMachine.currentIr === undefined){
        break}
      try {
        await this.step();
      } catch (error) {
        console.error('Error occurred:', error);
      }
    }
    this.wmMachine.wmStatus = WmStatus.ready
  }

  public async waitForButtonClick(): Promise<void> {
    this.waitingForClick = true;

    try {
      await new Promise<void>((resolve) => {
        const button = document.getElementById('mybutton');

        if (button) {
          const handleClick = () => {
            button.removeEventListener('click', handleClick);
            resolve();
          };

          button.addEventListener('click', handleClick);
        }
      });
    } finally {
      this.wmMachine.wmStatus = WmStatus.running
      this.waitingForClick = false;
    }
  }

  /**
   * Step Funktion schaut die Form Op | d | s | t.
   * Dann je nach dem Op_Code wird ein Instruction Register ausgeführt bzw. einen von 16_Step/Instruction.
   * Wenn einen Step ausgeführt, wird dann die Addes um Eins erhöht.
   */
  public async step(){
    let currentStatus = this.wmMachine.wmStatus

    this.wmMachine.currentIr = this.wmMachine.memory.get(this.wmMachine.currentPc)
    if(this.wmMachine.currentIr === undefined) {
      this.wmMachine.wmStatus = WmStatus.ready
      return}
    let value:number| undefined;

    const op = this.getOpCode(this.wmMachine.currentIr)
    switch (op) {
      default:
        console.log("default")
        this.wmMachine.wmStatus = WmStatus.ready
        break;
      case Instractions.add:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = Number(this.wmMachine.register[this.getS(this.wmMachine.currentIr)] ) + Number(this.wmMachine.register[this.getT(this.wmMachine.currentIr)])
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.sub:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = this.wmMachine.register[this.getS(this.wmMachine.currentIr)] - this.wmMachine.register[this.getT(this.wmMachine.currentIr)]
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.and:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = this.wmMachine.register[this.getS(this.wmMachine.currentIr)] & this.wmMachine.register[this.getT(this.wmMachine.currentIr)]
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.xor:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = this.wmMachine.register[this.getS(this.wmMachine.currentIr)] ^ this.wmMachine.register[this.getT(this.wmMachine.currentIr)]
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.shift_left:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = this.wmMachine.register[this.getS(this.wmMachine.currentIr)] << this.wmMachine.register[this.getT(this.wmMachine.currentIr)]
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.shift_right:
        let [signedValue]  = new Int16Array([this.wmMachine.register[this.getS(this.wmMachine.currentIr)]]);
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = (signedValue >> this.wmMachine.register[this.getT(this.wmMachine.currentIr)]);
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.load_address:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] =  this.getST(this.wmMachine.currentIr)
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.load:

        if(this.getST(this.wmMachine.currentIr) == 0xFF){
          console.log("load")
          if(this.waitingForClick){
            console.log("wating")
            break;
          }

          try {
            console.log("input")
            this.wmMachine.wmStatus = WmStatus.paused
            const childInput = document.getElementById('stdInput') as HTMLInputElement;
            childInput.value = ''
            alert("Bitte geben Sie Ihre Eingabe.")
            await this.waitForButtonClick();
            let userInput: string = '';
            if (childInput) {
              userInput = childInput.value;
            }
            this.wmMachine.memory.set(0xFF, userInput as unknown as number)
          } catch (error) {
            console.error('Error occurred:', error);
          }
          finally {
            this.wmMachine.wmStatus = WmStatus.running
            this.waitingForClick = false;
          }
        }

        value = this.wmMachine.memory.get(this.getST(this.wmMachine.currentIr))
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        if(value === undefined){
          alert("the address "+ this.getST(this.wmMachine.currentIr)+" in Memory is undifinde")
          this.wmMachine.currentIr = Instractions.halt;
          break;}
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = value
        break;
      case Instractions.store:
        this.wmMachine.memory.set(this.getST(this.wmMachine.currentIr),this.wmMachine.register[this.getD(this.wmMachine.currentIr)]);
        if(this.getST(this.wmMachine.currentIr) == 0xFF){
          const childInput = document.getElementById('stdout') as HTMLInputElement;
          if (childInput) {
            childInput.value = (this.wmMachine.memory.get(0xFF) as number ).toString(16).padStart(2, '0')
          }
        }
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.load_indirect:
        value = this.wmMachine.memory.get(this.wmMachine.register[this.getT(this.wmMachine.currentIr)])
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        if(value === undefined){
          alert("the address "+ this.wmMachine.register[this.getT(this.wmMachine.currentIr)] +" in Memory is undifinde")
          this.wmMachine.currentIr = Instractions.halt;
          break;}
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] =  value
        break;
      case Instractions.store_indirect:
        this.wmMachine.memory.set(this.wmMachine.register[this.getT(this.wmMachine.currentIr)], this.wmMachine.register[this.getD(this.wmMachine.currentIr)]);
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.branch_zero:
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        if(this.wmMachine.register[this.getD(this.wmMachine.currentIr)] === 0){
          this.wmMachine.currentPc = this.getST(this.wmMachine.currentIr);
        }
        break;
      case Instractions.branch_positive:
        let [signedInput]  = new Int16Array([this.wmMachine.register[this.getD(this.wmMachine.currentIr)] ]);
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        if(signedInput > 0){
          this.wmMachine.currentPc = this.getST(this.wmMachine.currentIr);
        }
        break;
      case Instractions.jump_register:
        this.wmMachine.currentPc = this.wmMachine.register[this.getD(this.wmMachine.currentIr)] & 0xFF;
        this.wmMachine.currentPc = this.wmMachine.currentPc + 1
        break;
      case Instractions.jump_and_link:
        this.wmMachine.register[this.getD(this.wmMachine.currentIr)] = this.wmMachine.currentPc;
        this.wmMachine.currentPc = this.getST(this.wmMachine.currentIr);
        break;
    }
    this.wmMachine.wmStatus = currentStatus;
    // console.log("out of switch")
  }

  /**
   * Reset Funktion wird (PC, ADDR, DATA, INSTR, REGISTER, currentPc, currentIr ) auf 0  bzw. auf Initialwerte zurückgesetzt
   */
  public reset(): void{
    this.initAddres();
    this.initPc();
    this.initInstr();
    this.initData();
    this.initRegister();
    this.wmMachine.memory = new Map<number, number>();
    this.wmMachine.wmStatus= WmStatus.ready;
    this.wmMachine.currentPc= 0x10;
    this.wmMachine.currentIr= 0x00;
  }

  /**
   * getDecimalValue Funktion repräsentiert die Nibble in Decimal Number
   * @param {Nibble} inputValue - inputValue Nibble param
   * @private
   * @return {number}  This is the result
   */
  public getDecimalValue(inputValue: Nibble[]): number{
    let result:number=0;
    for(let i = 0; i< inputValue.length ; i++){
      for(let j=0; j<inputValue[i].bits.length; j++){
        if(inputValue[i].bits[j].checked){
          result = result + inputValue[i].bits[j].value
        }
      }
    }
    return result;
  }


  public toHexadecimal(value: number, is16Bits: boolean): string {
    const maxLength: number  = is16Bits ? 4 : 2;
    return this.toBigEndian(value.toString(16).padStart(maxLength, '0'));

  }

  // Sort Algorithem für Memory_Addrese
  private toBigEndian(str:string): string{
    let result="";
    for(let i= str.length-1; i>=0 ;i--){
      result = result + str[i];
    }
    return result;
  }

  /**
   * storeValue Funktion speichert im Memory ein Adress mit dem entsprechenden Data
   * @param {MemoryValue} memoryValue - memoryValue MemoryValue param
   * @private
   */
  private storeValue(memoryValue: MemoryValue): void{
    this.wmMachine.memory.set(memoryValue.addres, memoryValue.data)
  }

  /**
   * getOpCode Funktion gibt zurück die OP_Code von die Data Form Op | d | s | t
   * Op | d | s | t jeder hat 4-Bit
   * @param {number} data - data ist number param
   * @private
   */
  private getOpCode(data: number):number{
    return (data >> 12 & 0xF);
  }

  /**
   * getD Funktion gibt zurück die d von die Data Form Op | d | s | t
   * Op | d | s | t jeder hat 4-Bit
   * @param {number} data - data ist number param
   * @private
   */
  private getD(data: number):number{
    return (data >> 8) & 0xF;
  }

  /**
   * getS Funktion gibt zurück die OP_Code von die Data Form Op | d | s | t
   * Op | d | s | t jeder hat 4-Bit
   * @param {number} data - data ist number param
   * @private
   */
  private getS(data: number):number{
    return (data >> 4) & 0xF;
  }

  /**
   * getT Funktion gibt zurück die t von die Data Form Op | d | s | t
   * Op | d | s | t jeder hat 4-Bit
   * @param {number} data - data ist number param
   * @private
   */
  private getT(data: number):number{
    return (data >> 0) & 0xF;
  }

  /**
   * getST Funktion gibt zurück die st von die Data Form Op | d | st
   * Op | d | st  , (Op,d) jeweils 4-Bit , st hat 8-Bit
   * OP | d werden mit Null gelegt.
   * St wird mit (& 0xFF) verknüpft.
   * @param {number} data - data ist number param
   * @private
   */
  private getST(data: number):number{
    return (data >> 0) & 0xFF;
  }

  /**
   *
   * updateWmPc Funktion überprügft dei 8-Bit Addres, falls ein Addres Bit auf Eins gesetzt,
   * dann wird die enscprechende PC CheckBox auf true gestetz.
   * PC und Addres haben die gleiche Strucktur
   * @param {number} addres - addres ist number param
   * @private
   */
  // Binary Darstellung Licht
  private updateWmPc(addres: number): void{
    for(let i= this.wmMachine.pc.length - 1 ; i >= 0; i--){
      for (let j = this.wmMachine.pc[i].bits.length-1; j >= 0 ; j--){
        if(this.wmMachine.pc[i].bits[j].value & addres){
          this.wmMachine.pc[i].bits[j].checked =  true;
        }
        else this.wmMachine.pc[i].bits[j].checked =  false;
      }
    }
  }

  /**
   * updateWmIr Funktion überprügft dei 16-Bit Data, falls ein Data Bit auf Eins gesetzt,
   * dann wird die enscprechende Instr CheckBox auf true gestetz.
   * Data und Instr haben die gleiche Strucktur
   * @param {number} data - data ist number param
   * @private
   */
// Binary Darstellung Licht
  private updateWmIr(data: number){
    for(let i= this.wmMachine.instr.length -1 ; i >= 0; i--){
      for (let j = this.wmMachine.instr[i].bits.length-1; j >= 0 ; j--){
        if(this.wmMachine.instr[i].bits[j].value & data){
          this.wmMachine.instr[i].bits[j].checked =  true;
        }
        else this.wmMachine.instr[i].bits[j].checked =  false;
      }
    }
  }

}
