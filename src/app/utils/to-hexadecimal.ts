import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'tohexadecimal'
})
export class ToHexadecimal implements PipeTransform{
  transform(value: number, is16Bits: boolean): string {
    const maxLength: number  = is16Bits ? 4 : 2;
    return this.toBigEndian(value.toString(16).padStart(maxLength, '0'));

  }

  // Sort Algorithem für Memory_Addrese
  private toBigEndian(str:string): string{
    let result="";
    for(let i= str.length-1; i>=0 ;i--){
      result = result + str[i];
    }
    return result;
}
}
