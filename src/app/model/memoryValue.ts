export class MemoryValue {
  addres: number
  data: number

  public constructor(addres:number, data: number) {
    this.addres = addres;
    this.data =  data;
  }
}
