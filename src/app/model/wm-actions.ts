export enum WmActions {
  load,
  look,
  step,
  run,
  reset
}
