export enum Instractions {
  halt,                 //   Halt
  add ,                 //   R[d] <- R[s] +  R[t]
  sub ,                 //   R[d] <- R[s] -  R[t]
  and,                  //   R[d] <- R[s] &  R[t]
  xor  ,                //   R[d] <- R[s] ^  R[t]
  shift_left,           //   R[d] <- R[s] << R[t]
  shift_right,          //   R[d] <- R[s] >> R[t]
  load_address ,        //   R[d] <- addr
  load ,                //   R[d] <- M[addr]
  store,                //   M[addr] <- R[d]
  load_indirect ,      //   R[d] <- M[R[t]]
  store_indirect,      //   M[R[t]] <- R[d]
  branch_zero    ,     //   if (R[d] == 0) PC <- addr
  branch_positive,     //   if (R[d] >  0) PC <- addr
  jump_register,       //   PC <- R[d]
  jump_and_link        //   R[d] <- PC; PC <- addr
}
