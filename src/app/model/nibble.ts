import {Bit} from "./bit";

export class Nibble {
  position: number;
  bits: Bit[] = new Array(4);

  public constructor(bits: Bit[], position: number) {
    this.bits = bits;
    this.position = position;
  }
}
