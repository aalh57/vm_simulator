export class Bit {
  value: number
  checked: boolean

  public constructor(value: number, checked: boolean) {
    this.value = value;
    this.checked = checked;
  }
}
