import {Nibble} from "./nibble";
import {Instractions} from "./instractions";
import {WmStatus} from "./wm-status";


export class WmMachine {
  public pc: Nibble[] = new Array(2) ;
  public addres: Nibble[] = new Array(2) ;
  public instr: Nibble[] = new Array(4) ;
  public data: Nibble[] = new Array(4) ;
  public memory: Map<number, number> = new Map<number,number>();
  public currentPc: number = 0x10;
  public currentIr: number| undefined = Instractions.halt;
  public register: number[] = new Array(16);
  public wmStatus: WmStatus = WmStatus.ready;

  public constructor() {
    this.pc = new Array(2) ;
    this.addres = new Array(2) ;
    this.instr = new Array(4) ;
    this.data = new Array(4) ;
    this.memory = new Map<number,number>();
    this.currentPc = 0x10;
    this.currentIr = Instractions.halt;
    this.register = new Array(16);
  }
}
