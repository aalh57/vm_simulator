import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {BinaryInputComponent} from "./binary-input/binary-input.component";
import { TabComponent } from './tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';
import {RouterLink, RouterModule} from "@angular/router";
import { HilfeComponent } from './hilfe/hilfe.component';
import {AppRoutingModule} from "./app-routing.module";
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    BinaryInputComponent,
    TabComponent,
    TabsComponent,
    HilfeComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterLink,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
