import {WmActions} from './model/wm-actions'; // Import necessary classes/enums
import {DashboardComponent} from "./dashboard/dashboard.component";
import {fakeAsync, tick} from "@angular/core/testing";

describe('InstructionTest',()=>{
  let app: DashboardComponent;
  beforeEach(() => {
    app = new DashboardComponent();
    // Additional setup if needed
    app.ngOnInit();
  });

  it('Add', fakeAsync( ()=>{
    app.loadList(new Map<string,string>(
        [
          ['10','8A15'],
          ['11','8B16'],
          ['12','1CAB'],
          ['13','9C17'],
          ['14','0000'],
          ['15','0005'],
          ['16','0004'],
          ['17','0000']
        ]));
    app.runWmAction(WmActions.run)
    tick();
    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(9);
  }))
  it('Sub', fakeAsync(()  =>{
    app.loadList(new Map<string,string>(
      [
        ['10','8A15'],
        ['11','8B16'],
        ['12','2CAB'],
        ['13','9C17'],
        ['14','0000'],
        ['15','0008'],
        ['16','0005'],
        ['17','0000']
      ]));
    app.runWmAction(WmActions.run)
    tick(); // Simulate the passage of time until all pending asynchronous activities are resolved


    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(3);
  }))
  it('should be  Fibonacci of 13',fakeAsync( () => {
    app.loadList(new Map<string, string>([
      ['40', '7101'],
      ['41', '7A00'],
      ['42', '7B01'],
      ['43', '894C'],
      ['44', 'C94B'],
      ['45', '9AFF'],
      ['46', '1CAB'],
      ['47', '1AB0'],
      ['48', '1BC0'],
      ['49', '2991'],
      ['4A', 'C044'],
      ['4B', '0000'],
      ['4C', '000C']
    ]));
  app.wmMachine.currentPc=64
    app.runWmAction(WmActions.run)
    tick();
  // Simulate the passage of time until all pending asynchronous activities are resolved

    expect(app.wmMachine.memory.get(parseInt("FF",16))).toEqual(89);
  }));

  it('bitwise AND  ', fakeAsync (() => {

    app.loadList(new Map<string,string>(
      [
        ['10','8A15'],
        ['11','8B16'],
        ['12','3CAB'],
        ['13','9C17'],
        ['14','0000'],
        ['15','0005'],
        ['16','0006'],
        ['17','0000']
      ]));
    app.runWmAction(WmActions.run)

    tick()
    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(4);
  }));

  it('bitwise OR  ', fakeAsync (() => {

    app.loadList(new Map<string,string>(
      [
        ['10','8A15'],
        ['11','8B16'],
        ['12','4CAB'],
        ['13','9C17'],
        ['14','0000'],
        ['15','0005'],
        ['16','0006'],
        ['17','0000']
      ]));
    app.runWmAction(WmActions.run)

    tick()
    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(3);
  }));

  it('schift left ', fakeAsync (() => {

    app.loadList(new Map<string,string>(
      [
        ['10','8A15'],
        ['11','8B16'],
        ['12','5CAB'],
        ['13','9C17'],
        ['14','0000'],
        ['15','0004'],
        ['16','0002'],
        ['17','0000']
      ]));

    app.runWmAction(WmActions.run)

    tick()
    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(16);
  }));

  it('schift right ', fakeAsync (() => {

    app.loadList(new Map<string,string>(
      [
        ['10','8A15'],
        ['11','8B16'],
        ['12','6CAB'],
        ['13','9C17'],
        ['14','0000'],
        ['15','0004'],
        ['16','0001'],
        ['17','0000']
      ]));
    app.runWmAction(WmActions.run)

    tick()
    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(2);
  }));
  it('ggt test branch postive', fakeAsync(() => {
    // Load memory with an address and a value at that address
    app.loadList(new Map<string, string>([
      ['20', '8A2B'],
      ['21', '8B2C'],
      ['22', '2CAB'],
      ['23', 'CC29'],
      ['24', 'DC27'],
      ['25', '2BBA'],
      ['26', 'C022'],
      ['27', '2AAB'],
      ['28', 'C022'],
      ['29', '9A2D'],
      ['2A', '0000'],
      ['2B', '00C3'],
      ['2C', '0111'],
      ['2D', '0000']
    ]));
    app.wmMachine.currentPc=0x20
    app.runWmAction(WmActions.run)
    tick()
    expect(app.wmMachine.memory.get(parseInt("2D",16))).toEqual(0x27);
  }));
});

/*

describe('InstructionTest',()=>{
  let app = new AppComponent()
  //  let binaryInputComponent = new BinaryInputComponent()
  app.ngOnInit()
  it('Jump',(()=>{
    app.loadList(new Map<string,string>(
    [10: 7101]
    [11: 82FF]
    [12: 7301]

    13: 1A30
    14: 1B20
    15: FF30
    16: 13C0
    17: 2221
    18: D213
    19: 93FF
    1A: 0000
    30: 7C00
    31: 7101
    32: CA36
    33: 1CCB
    34: 2AA1
    35: C032
    36: EF00
    FF: 0005
    app.runWmAction(WmActions.run)
    expect(app.wmMachine.memory.get(parseInt("17",16))).toEqual(3);
  }))
}); */
