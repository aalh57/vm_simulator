# Change Log

All notable changes to this project will be documented in this file.


## [Unreleased]

### Added

-
-   

### Changed

- 
- 

### Deprecated

- 

### Removed

- 

### Fixed

-

## [2.0.0] - 2023-11-05

### Added

- saveFile Function.
- Allow comments in text and load text .
- Add CHANGELOG.md .
- write "Berechnungsmodelsimulator" instead of BEMOS to the title in the UI

### Changed

- Updates to existing Functions like getAdjustedList or isHexadecimal.
- Updates UI design like Text-Speicher-Buttom and Title name
### Deprecated

- None.

### Removed

- None

### Fixed

- few tasks from Smaller TODOs


