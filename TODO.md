# TODO

## Larger TODOs (aka. core parts of the bachelor thesis project)
- [x] add unit tests for commands and small programs
- [x] perform user tests, document and incorporate results.

## Smaller TODOs
- [x] Standardein - und -Ausgabe
- [x] Add configurable version number to program (configurable to be shown in the
      UI). Bspw. `versions_number.xml`
- [x] Add proper `CHANGELOG.md` file
- [x] Improve program's error messaged (e.g. when entering a program with only the line `10: 8000`)
- [x] Translate complete UI into German. Currently it is mixed
- [x] add configurable Link to repository in UI. Maybe confiruable system constants file.
- [x] write "Berechnungsmodelsimulator" instead of BEMOS to the title in the UI
- [x] add "Cheat Sheet" to the UI. Preferably taken from a single text source file that is
      part of the repository. Similar to the TOY program.
- [ ] Allow comments after command in the text (like the original TOY program)
- [ ] Insert comments automatically into the text, if there are none (like the original
      TOY program)
- [ ] comments should be supported in file loading
- [x] add file save function (saves then with comments inserted by the system)
- [ ] move everything to a project repository independent of THM user name
- [ ] Change URL to something related to BEMOS. So far it is https://aalh57.git-pages.thm.de/vm_simulator/
- [ ] add proper `LICENSE.md` file as discussed
## to be discussed TODOs
- [ ] Output errors if program does not end with 0000. For this internally the memory
      cells with `undefined` instead of 0000.


# things related to the Bachelor thesis

- [ ] Codewalkthrough machen
- [ ] User tests discussion

## Inhalt der BA

- ?

## Literaturarbeit

- Referenzen
  - von Neumann Simulator
  - from nand to tetris und das zugehörige Buch
